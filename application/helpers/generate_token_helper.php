<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
   if(!function_exists('generateToken')){
     function generateToken($length){
     $key='';
     $keys = array_merge(range(0,9), range('a', 'z'));
     for($i =0; $i < $length; $i++){
      $key .=$keys[array_rand($keys)];
     }
     return $key;
    }   
   }

