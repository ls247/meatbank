<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
   if(!function_exists('generateAccountNumber')){
     function generateAccountNumber($length){
     $key='';
     $keys = range(0,9);
     for($i =0; $i < $length; $i++){
      $key .=$keys[array_rand($keys)];
     }
     return 'MB-'.$key;
    }   
   }

