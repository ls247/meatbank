<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
   if(!function_exists('hashPassword')){
     function hashPassword($password){
        return password_hash($password, PASSWORD_DEFAULT);
    }  
   }  
  
  
