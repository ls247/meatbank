<?php
class Meatbank_model extends CI_Model {

    protected $users_table = "users";
    protected $roles_table = "roles";
    protected $meat_table = "meat";
    protected $deposit_table = "deposit";
    protected $withdrawal_table = "withdrawal";
    protected $states_table = "states";   
    protected $locations_table = "locations"; 
    protected $bag_codes_table = "bag_codes";               
    protected $sms_table = 'sms';
    protected $sms_withdrawal_table = 'sms_withdrawal';

    protected $superAdminEmail ="admin@adminlivestock247.com";
    protected $superAdminPassword = "desk_123";
    protected $superAdminRole = 1;
    protected $superAdminStatus = 'verified';
    
    public function createSuperAdmin(){
        $this->db->where("email",$this->superAdminEmail);
        $this->db->limit(1);
        $query = $this->db->get($this->users_table);
        if ($query->num_rows() == 1){
            return true;
        }else{
            $insert_data= array( "email"=> $this->superAdminEmail, "password"=> hashPassword($this->superAdminPassword), "role_id"=> $this->superAdminRole, "status" => $this->superAdminStatus);
            $this->db->insert($this->users_table, $insert_data);
            return true;
        }
    }

    public function createState(){
        $file = 'assets/states-localgovts/states-localgovts.json';
        $data = file_get_contents($file);
        $jsondata = json_decode($data, true);
        $states = $jsondata['states'];
        foreach($states as $state){
            $this->db->where("state_name",$state['state']);
            $this->db->limit(1);
            $query = $this->db->get($this->states_table);
            if ($query->num_rows() == 1){
                return false;
            }else{                        
                $insert_data= array( "state_name"=> $state['state'],);
                $this->db->insert($this->states_table, $insert_data);
            }                      
        }  
    }

    public function get_states() {        
        $this->db->select('*');
        $this->db->from($this->states_table);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else{
            return false;
        }
    }

    public function signup($fullname, $email, $phone_number, $account_number, $password, $status, $role)
    {
        $this->db->where("email",$email);
        $this->db->limit(1);
        $query = $this->db->get($this->users_table);
        if ($query->num_rows() == 1){
            return false;
        }else{
            $insert_data= array( "name"=> $fullname, "email"=> $email, "phone"=> $phone_number, "account_number"=> $account_number, "password"=> $password, "status"=> $status,  "role_id"=> $role);
            $this->db->insert($this->users_table, $insert_data);
            return true;
        }
    }

    public function update_token($token, $user_id) {
        $data = array('token'=>$token);
        $this->db->set($data)->where("id", $user_id)->update($this->users_table);
        return true;
    }

    public function verify($qstring, $status) {
        $data = array('token'=>$qstring, 'status'=>$status);
        $this->db->set($data)->where("token", $qstring)->update($this->users_table);
        return true;
    }

    public function delete_account($user_id) {
        $this->db->where("id",$user_id);
        $this->db->delete($this->users_table);
        return true;
    }

    public function login_account($username, $password)
    {
        $this->db->select('*');
        $this->db->from($this->users_table);
        $this->db->where("email",$username);
        $this->db->or_where("phone", $username);
        $this->db->or_where("account_number", $username);      
        $this->db->limit(1);
        $query = $this->db->get();
        $result=$query->result();
        if(isset($result) && $result[0]->status=="verified" && password_verify($password, $result[0]->password) ){
            return true;                 
        }else{       
            return false;     
        }
    }

    public function check_available_kg($withdrawal_code){
        $conditions = array("withdrawal_code"=>$withdrawal_code); 
        $this->db->select('*');
        $this->db->from($this->deposit_table);
        $this->db->where($conditions);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result[0]->reconsilation_kg;
        }
    }
    public function user_information($username) {
        $this->db->select('*');
        $this->db->from($this->users_table);
        $this->db->where("email",$username)->or_where("phone", $username)->or_where("account_number", $username);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function get_role($role_id) {
        $this->db->select('*');
        $this->db->from($this->roles_table);
        $this->db->where("id",$role_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result[0]->role;
        } else {
            return false;
        }
    }

    public function get_user($user_id){    
        $this->db->select('id, name, email, phone, account_number');
        $this->db->from($this->users_table);
        $this->db->where("id",$user_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result[0];
        } else {
            return false;
        }                
    }

    public function get_user_meat_deposit($user_id,$withdrawal_code){ 
        $conditions = array("user_id"=> $user_id, "withdrawal_code"=>$withdrawal_code); 
        $this->db->select('*');
        $this->db->from($this->deposit_table);
        $this->db->where($conditions);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result[0];
        } else {
            return false;
        }                
    } 

    public function update_user($user_id, $phone_number){
        $data = array('phone'=>$phone_number);
        $this->db->set($data)->where("id", $user_id)->update($this->users_table);                                
        return true;
    }                
    public function get_meat_types() {
        $this->db->select('*');
        $this->db->from($this->meat_table);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else{
            return false;
        }
    }
    public function check_account_number_phone($account_number_or_phone){ 
        $this->db->where("account_number",$account_number_or_phone);
        $this->db->or_where("phone", $account_number_or_phone);
        $this->db->limit(1);
        $query = $this->db->get($this->users_table);
        if ($query->num_rows() == 1) {
            return true;
        }else {
            return false;
        }        
    }
    public function check_account_number($account_number){ 
        $this->db->where("account_number",$account_number);
        $this->db->limit(1);
        $query = $this->db->get($this->users_table);
        if ($query->num_rows() == 1) {
            return true;
        }else {
            return false;
        }
    }

    public function check_withdrawal_code($withdrawal_code){ 
        $this->db->where("withdrawal_code",$withdrawal_code);
        $this->db->limit(1);
        $query = $this->db->get($this->deposit_table);
        if ($query->num_rows() == 1) {
            return true;
        }else {
            return false;
        }
    }

    public function insert_withdrawal($user_id, $deposit_id, $paystack_ref,  $transaction_time, $kg_withdrawn, $status, $amount_paid)
    {
        $insert_data= array( "user_id"=> $user_id, "kg_withdrawn"=> $kg_withdrawn, "deposited_id"=> $deposit_id, "status"=> $status, "amount"=> $amount_paid, 
        "paystack_ref"=> $paystack_ref, "transaction_time"=> $transaction_time);
        $this->db->insert($this->withdrawal_table, $insert_data);
        return true;                                
    }

    public function update_deposit_withdrawal($user_id, $withdrawal_code, $total_kg_left, $transaction_time, $address){
        $conditions =$data = array('user_id'=>$user_id, 'withdrawal_code'=>$withdrawal_code);
        $data = array('transaction_time'=>$transaction_time, 'reconsilation_kg'=> $total_kg_left, 'address'=> $address);
        $this->db->set($data)->where($conditions)->update($this->deposit_table);
        return true;
    }  
                    
    public function meat_deposit($user_id, $size, $total, $meat_type, $center, $bag_code_id, $days, $service, $status, $address){
        $insert_data= array( "user_id"=> $user_id, "kg"=> $size, "reconsilation_kg"=> $size, "amount"=> $total, "meat_type_id"=> $meat_type, "center"=> $center, "bag_code_id"=> $bag_code_id,"days"=> $days, "service"=> $service, "status"=> $status, "address"=> $address);
        $this->db->insert($this->deposit_table, $insert_data);
        $data = array('status'=> 'used');
        $this->db->set($data)->where("id", $bag_code_id)->update($this->bag_codes_table);
        return true;             
    }

    public function update_deposit($user_id, $bag_code_id, $paystack_ref, $withdrawal_code, $transaction_time, $status, $address) {
        $conditions =$data = array('user_id'=>$user_id, 'bag_code_id'=>$bag_code_id);
        $data = array('user_id'=>$user_id, 'paystack_ref'=>$paystack_ref, 'withdrawal_code'=>$withdrawal_code, 'transaction_time'=>$transaction_time, 'status'=> $status, 'address'=> $address);
        $this->db->set($data)->where($conditions)->update($this->deposit_table);
        return true;
    }
    
    public function delete_deposit($user_id) {
        $this->db->where("user_id",$user_id);
        $this->db->delete($this->deposit_table);
        return true;
    }
    
    public function get_random_bag_code($center){
        $this->db->select('id');
        $this->db->where(array('meat_bank_location_id'=>$center));
        $this->db->where('status', NULL);
        $this->db->order_by('rand()');
        $this->db->from($this->bag_codes_table);        
        $query = $this->db->get();
        $result = $query->result();
        if($result){
            return $result[0]->id;
        }else{
            return '';
        }
    }

    public function get_bag_code($bag_code_id){
        $this->db->select('bag_code_name');
        $this->db->where(array('id' => $bag_code_id));
        $this->db->from($this->bag_codes_table);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->bag_code_name;
    }

    public function get_bag_code_level($center){
        $this->db->select('bag_code_name');
        $this->db->where(array('status !='=>'used', 'meat_bank_location_id'=>$center));
        $query = $this->db->get($this->deposit_table);
        return $query->num_rows();
    }     

    public function get_deposit($user_id, $bag_code_id){
        $conditions = array("user_id"=> $user_id, "bag_code_id"=> $bag_code_id); 
        $this->db->select('*');
        $this->db->from($this->deposit_table);
        $this->db->where($conditions);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result[0];
        } else {
            return 0;
        }                
    }

    public function get_meat($meat_type_id){
        $this->db->select('*');
        $this->db->from($this->meat_table);
        $this->db->where("id",$meat_type_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result[0];
        } else {
            return 0;
        }                
    }
    
    public function send_sms_withdraw($user_id, $fullname, $phone_number, $kg_withdrawn, $total_kg_left, $meat_type, $amount_paid, $meat_bank_location){
        $sms_api_key = 'TLLXf8lLQZpsvuFouxWoN89YzoxL23RyXDUtDKAgNcniDpgGdpMUkgqxilO0tW';
        //$sms_api_key = 'TLaZcGhbQBk5XH89jpO4mqAXlT6gS34heRzDu0LRaqaCRHlZ8MMm33YKjVzmCF';
        if($total_kg_left == 0){
            $sms_message = 'Hello '.ucwords($fullname).' you do not have any kg of '.$meat_type.' left in any of our meat bank center';
        }else{
            if($service=="dropoff"){
                $sms_message = 'Hello '.ucwords($fullname).' you have just withdrawn '.$kg_withdrawn.'kg of'.$meat_type.' and your total kg worth of '.$meat_type.' left is'.$total_kg_left.'kg which will be delivered to you by our '.$meat_bank_location.' meat bank center';
            }
            if($service=="pickup"){
                $sms_message = 'Hello '.ucwords($fullname).' you have just withdrawn '.$kg_withdrawn.'kg of'.$meat_type.' and your total kg worth of '.$meat_type.' left is'.$total_kg_left.'kg we will be expecting you at our meat bank center for collection';
            }
    
        }

        $sms_message .= 'Meatbank Powered By Livestock247.com';
        $recipient = 'To Our Meatank Customer At livestock27 '.ucwords($fullname);
        $payload = array(   'to'=>'234'.ltrim($phone_number, '0'),'from'=>'fastbeep','sms'=>$sms_message,'channel'=> 'generic','type'=>'plain','api_key'=>$sms_api_key);
        $post_data = json_encode($payload);   
        if (isset($phone_number) && !empty($phone_number)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.ng.termii.com/api/sms/send',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                //CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYPEER => false,
                //CURLOPT_CAINFO, "C:/xampp/cacert.pem",
                //CURLOPT_CAPATH, "C:/xampp/cacert.pem",
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>$post_data,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json')
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $res = json_decode($response, true);
            
            if($err){
                echo 'error';
                echo $err;
                return false;
            }else{
                if($response){
                    $data = array('message' => $sms_message,'date' => date('Y-m-d H:i:sa'),'recipient' => $recipient,'user' => $user_id);
                    $this->insertSmsWithdrawal($data);                
                    echo "sent";
                    return true;
                }else{
                    echo "notSent";
                    return false;
                }
            }           
        } else{
            echo 'phone number not available';
            return false;
        }		
    } 

    public function insertSmsWithdrawal($data){
        $this->db->insert($this->sms_withdrawal_table, $data);
    }

    public function send_sms( $user_id, $fullname, $phone_number, $size, $days, $meat_bank_center, $withdrawal_code){
        $sms_api_key = 'TLLXf8lLQZpsvuFouxWoN89YzoxL23RyXDUtDKAgNcniDpgGdpMUkgqxilO0tW';
        //$sms_api_key = 'TLaZcGhbQBk5XH89jpO4mqAXlT6gS34heRzDu0LRaqaCRHlZ8MMm33YKjVzmCF';
        $sms_message = 'Kindly use this widthdrawal code '.$withdrawal_code.' to collect your meat worth of'.$size.'kg at meatbank center '.ucfirst($meat_bank_center).' after '.$days.'day/days from now of keeping your meat with us.'. "\r\n";
        $sms_message .= 'Meatbank Powered By Livestock247.com';
        $recipient = 'To Our Meatank Customer At livestock27 '.ucwords($fullname);
        $payload = array('to'=>'234'.ltrim($phone_number, '0'),'from'=>'fastbeep','sms'=>$sms_message,'channel'=> 'generic','type'=>'plain','api_key'=>$sms_api_key);
        $post_data = json_encode($payload);   
                        
        if (isset($phone_number) && !empty($phone_number)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.ng.termii.com/api/sms/send',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                //CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYPEER => false,
                //CURLOPT_CAINFO, "C:/xampp/cacert.pem",
                //CURLOPT_CAPATH, "C:/xampp/cacert.pem",
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>$post_data,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json')
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $res = json_decode($response, true);
            
            if($err){
                echo 'error';
                echo $err;
                return false;
            }else{
                if($response){
                    $data = array('message' => $sms_message,'date' => date('Y-m-d H:i:sa'),'recipient' => $recipient,'user' => $user_id);
                    $this->insertSms($data);                
                    echo "sent";
                    return true;
                }else{
                    echo "notSent";
                    return false;
                }
            }
        }else{
            echo 'phone number not available';
            return false;
        }		
    } 

    public function insertSms($data){
        $this->db->insert($this->sms_table, $data);
    }

    public function get_count($user_id){
        return $this->get_count_of_orders($user_id);
    }

    public function get_count_of_orders( $user_id){
        $conditions = array("user_id"=> $user_id, "status"=>"pending");
        $this->db->where($conditions);
        $query = $this->db->get($this->deposit_table);
        return $query->num_rows();
        
    }

    public function get_meat_bank_locations($state_id) {
        $condition = array("state_id"=> $state_id);
        $this->db->where($condition);
        $query = $this->db->get($this->locations_table);
        $output = '';
        if ($query->num_rows() == 0) {
            $output = true;
        }else{
            $meat_bank_centers = $query->result();
            $output = '';
            foreach($meat_bank_centers as $meat_bank_center){
                $output .="<option  value='$meat_bank_center->id'>".ucfirst($meat_bank_center->location_name)."</option>";
            }
        }
        return $output;
    }  

    public function get_location_details($center){
        $this->db->select('*');
        $this->db->where(array('id' => $center));
        $this->db->from($this->locations_table);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0];                    
    }

    public function get_all_orders($limit, $start, $user_id){
        $output = '';
        $condition = array("user_id"=> $user_id, "status"=>"pending");
        $this->db->limit($limit, $start);
        $this->db->where($condition);
        $this->db->order_by("user_id", "ASC");
        $query = $this->db->get($this->deposit_table);
        $output .='
        <table class="table table-stripe" style="text-align:center; width:100%; border-bottom:10px solid #EBEBF2">
            <thead>   
                <tr>
                    <th>S/n</th>
                    <th>Amount</th>
                    <th>Size (Kg)</th>                                
                    <th>Type Of Meat</th>
                    <th>Duration (Days)</th>
                    <th>Center Deposited</th>
                    <th>Type Of Service</th>
                    <th>Order Status</th>
                    <th>Action</th>
                </tr>
            </thead>';                   
        if ($query->num_rows() > 0) {
            $orders= $query->result();
            $i= 1;
            foreach ($orders as $order)
            {
                $id =$order->id;
                $days =$order->days;
                $center = $order->center;
                if($days > 1){
                $days =$days.' Days';
                }else{
                    $days =$days.' Day';
                }
                if($i % 2 == 0){
                    $scope = "info"; 
                }
                else{
                    $scope = "active";
                }
                $location_center_details = $this->get_location_details($center);
                $meat_bank_center =$location_center_details->location_name;
                $output .='<tbody>
                <tr class="'.$scope.'">
                    <td>'.$i.'</td>
                    <td> &#8358 '.$order->amount.'</td>
                    <td>'.$order->kg.' (Kg)</td>';
                    $condition = "id =" . "'" . $order->meat_type_id. "'";
                    $this->db->where($condition);
                    $this->db->limit(1);
                    $query = $this->db->get($this->meat_table);        
                    $meat = $query->result();
                    $output .='<td>'.$meat[0]->meat_type.'</td>
                                <td>'.$days.'</td>
                                <td>'.ucfirst($meat_bank_center).'</td>
                                <td>'.ucfirst($order->service).'</td>
                                <td>'.ucfirst($order->status).'</td>
                                <td style="width:150px; margin-left:20px;text-align:center">
                                    <span  style="text-align:center;"><a href="javascript:void(removeOrder())" id="'.$id.'" class="deleteOrder" data-confirm="Are you sure you want to remove this pending order?" title="Delete Meat Deposit Order" style="text-decoration:none"><i  class="fa fa-trash fa-lg" aria-hidden="true"></i> Remove</a></span>                                        
                                </td >                                
                </tr></tbody>';
                $i++;
            }   
            $output .='</table>';       
        } else {
            $output ='<div style="text-align:center">No Meat Currently Deposited</div>';             
        } 
    
        return $output;    
    } 
    
    public function delete_deposit_order($depositId) {
        //$this->db->where("id", $depositId);
        //$this->db->delete($this->deposit_table);
        $condition = array('id'=>$depositId);
        $data = array('status'=>'delete',);
        $this->db->set($data)->where($condition)->update($this->deposit_table);       
        return true;                
    }              
      
}