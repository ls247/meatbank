<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home - Preserve your meat with 24 hours power supply | Livestock247</title>
    <!-- plugins:css -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<link href="https://livestock247.com/assets/css/font-awesome.min.css" rel="stylesheet">	
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/Fira_Sans/FiraSans-ExtraLight.ttf')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/iconfonts/ionicons/css/ionicons.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/iconfonts/typicons/src/font/typicons.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/iconfonts/font-awesome/css/font-awesome.min.css')?>">       
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.base.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.addons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">		

    <!-- End Layout styles -->
    <link rel="shortcut icon"  href="<?php echo base_url('assets/images/MeatBank.png')?>" />
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js')?>"></script>
    <script>
		var i = 0;
		var text = "We keep It Safe, Fresh and Healthy With Our Solar Powered Cold Room";
		var speed = 120;
		function typeEffect(){
			if(i < text.length) {
			document.getElementById("main-heading").innerHTML += text.charAt(i);
			i++;
			setTimeout(typeEffect, speed);
			}
		}
	   	function covertKilo() {

		  document.getElementById('size-group').innerHTML = '';
		  var pricePerUnit = document.getElementById('size').value;
		  var kiloSize = document.getElementById('size').value;
		  var days = document.getElementById('days').value;
		  if(isNaN(kiloSize) || kiloSize == '' || kiloSize == 0 ) {
			document.getElementById('kilo').classList.add('has-error');
			document.getElementById('size-group').style.display = 'block';
			document.getElementById('size-group').style.color = 'maroon';
			document.getElementById('size-group').style.fontSize = 'small';
			document.getElementById('size-group').innerHTML = 'Meat size is needed';
		  }else{
			document.getElementById('kilo').classList.remove('has-error');
			document.getElementById('size').value = kiloSize;
			var unitPerDays =document.getElementById('perUnit').value;			
			var total =kiloSize*days*unitPerDays;												  
			document.getElementById('total').value =total;			
		  }
	  }
	  
	  function covertDays() {
		  document.getElementById('days-group').innerHTML = '';
		  var kiloSize = document.getElementById('size').value;
		  var days = document.getElementById('days').value;
		  
		
		  if(isNaN(kiloSize) || kiloSize == '' || kiloSize == 0) {
			document.getElementById('kilo').classList.add('has-error');			
			document.getElementById('size-group').style.display = 'block';
			document.getElementById('size-group').style.color = 'maroon';
			document.getElementById('size-group').style.fontSize = 'small';
			document.getElementById('size-group').innerHTML = 'Meat size is needed';
			document.getElementById('total').value = 0;
		  }else if(isNaN(days) || days == '' || days == 0) {
			document.getElementById('kilo').classList.remove('has-error');
			document.getElementById('day').classList.add('has-error');	
			document.getElementById('days-group').style.display = 'block';
			document.getElementById('days-group').style.fontSize = 'small';
			document.getElementById('days-group').style.color = 'maroon';
			document.getElementById('total').value = 0;
			document.getElementById('days-group').innerHTML = 'Please specify number of days';
		  }else{
			document.getElementById('day').classList.remove('has-error');	
			var unitPerDays =document.getElementById('perUnit').value;			
			var total = kiloSize*days*unitPerDays;												  
			document.getElementById('total').value =total;															  
			  
		  }
	  }
 
	  function addAddress() {
		  document.getElementById('addAddress').style.display = 'block';
		  var center = document.getElementById('deposit_center').value;
		  document.getElementById('deposit-meat-center-group').style.display = 'none';
		  if(  center == 'none') {
			document.getElementById('addAddress').style.display = 'none';
			document.getElementById('deposit-meat-center-group').style.display = 'block';
			document.getElementById('deposit-meat-center-group').style.fontSize = 'small';
			document.getElementById('deposit-meat-center-groupp').style.color = 'maroon';
			document.getElementById('deposit-meat-center-group').innerHTML = 'Select a meat bank center';
		  }
	  }

       $(document).ready(function(){
		   	window.onload = typeEffect();
			$('#deposit-block').hide('fast');
			$('#deposit-details').hide('fast');			   
		   	$('#deposit-payment').hide('fast'); 
			$('#deposit-message').hide('fast'); 			
			$('.pickup_address').hide('fast');
			$('.pickup_charge').hide('fast');	
			$('#service').click(function(){
				if($('#service').val() =="pickup"){
					$('#addAddress').show('fast');
				}
				if($('#service').val() =="dropoff"){
					$('#addAddress').hide('fast');
				}
			});

			$("#deposit").on('submit',function(e){
				e.preventDefault(); 
			  $('.accountNumber-group').removeClass('has-error');
			  $('.kilogram-group').removeClass('has-error');
			  $('.perUnit-group').removeClass('has-error');
			  $('.days-group').removeClass('has-error');
			  $('.total-group').removeClass('has-error');
			  $('.meat-type-group').removeClass('has-error');
			  $('.service-group').removeClass('has-error');
			  $('.deposit-state-group').removeClass('has-error');
			  $('.deposit-meat-center-group').removeClass('has-error');
              $('.userPassword-group').removeClass('has-error');
			  $('.address-group').removeClass('has-error');
			  $('.deposit-total').removeClass('has-error');
              $('.help-block').remove();
			  var form_data = new FormData(this);
			  var accountNumber = $("input[name='account_number']").val();
			  var size = $("input[name='size']").val();
			  var perUnit = $("input[name='perUnit']").val();
			  var days = $("input[name='days']").val();
			  var total = $("input[name='total']").val();
			  var meatType = $("#meat_type").val();
			  var service = $("#service").val();
			  var state = $("#deposit_state").val();
			  var center = $("#deposit_center").val();
			  var address = $("#address").val();
                  $.ajax({
                        url: "<?php echo site_url() ?>post_deposit",
                        method: 'POST',
						data: form_data,
						processData: false,
						contentType: false,
						cache: false,                         
					   dataType: 'json'
                   }).done(function (data){
					if(data.failure){
						if(data.response=="locked"){
							$('#deposit-block').show('fast');
							$('#deposit-block').html('<div class="col-md-12 text-danger">You can\'t deposit your meat now contact  Livestock247 Meatbank</div><br><br>');
							exit;
						}
						
                        window.location.replace('<?php echo site_url(); ?>login');
                    }else if(!data.success){
                        if(data.error.account_number){
							$('.accountNumber-group').addClass('has-error');
							$('.accountNumber-group').append('<div class="help-block text-danger" style="font-size:small">' + data.error.account_number + '</div>'); // add the actual error message under our input
                        }else{
							if(data.error.size){
								$('.size-group').addClass('has-error');
								$('.size-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.size + '</div>'); // add the actual error message under our input
							}else{
								if(data.error.days){
									$('.days-group').addClass('has-error');
									$('.days-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.days + '</div>'); // add the actual error message under our input
								}else{
									if(data.error.meat_type){
										$('.meat-type-group').addClass('has-error');
										$('.meat-type-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.meat_type + '</div>'); // add the actual error message under our input
									}else{
										if(data.error.service){
											$('.service-group').addClass('has-error');
											$('.service-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.service + '</div>'); // add the actual error message under our input
										}else{
											if(data.error.state){
												$('.deposit-state-group').addClass('has-error');
												$('.deposit-state-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.state + '</div>'); // add the actual error message under our input
											}else{
												if(data.error.center){
													$('.deposit-meat-center-group').addClass('has-error');
													$('.deposit-meat-center-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.center + '</div>'); // add the actual error message under our input
												}else{
													if(data.error.address){
														$('.address-group').addClass('has-error');
														$('.address-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.address + '</div>'); // add the actual error message under our input
													}else{
														$('.deposit-total').addClass('has-error');
														$('.deposit-total').prepend('<div class="help-block error text-danger" style="font-size:small">' + data.error.total + '</div>'); // add the actual error message under our input									  
													}									
												}	
								  			}
										}								
							  		}
								}
						  	}
                        }
                    }else{
						if(data.response=="deposit"){
							$('#depositModal').hide('fast');
							window.location.replace('<?php echo site_url(); ?>deposit');
						}else if(data.response=="main"){
							$('#depositModal').hide('fast');
							window.location.replace('<?php echo site_url(); ?>/');
						}						
                    }    
          
                 });
            });

			//withdrawal Ajax
				$('#add-address').hide('fast');
				$('#withdraw_service').click(function(){
					if($('#withdraw_service').val() =="pickup"){
						$('#optional_address').hide('fast');
					}
					if($('#withdraw_service').val() =="dropoff"){
						$('#optional_address').show('fast');
					}
				})
				$('#address_change').click(function(){
					if($('#address_change').is(':checked')){
						$('#add-address').show('fast');
					}else{
						$('#add-address').hide('fast');
					}
				})	
				$("#withdrawal_address").hide('fast');
				$("#withdrawal-details").hide('fast');
				$('#payment-button').hide('fast'); 
				$('#withdrawal-button').hide('fast'); 
				$("#withdraw").on('submit',function(e){
				e.preventDefault(); 
				$('.withdraw-account-number-or-phone-group').removeClass('has-error');
				$('.withdraw-code-group').removeClass('has-error');			  
				$('.withdraw-kilogram-group').removeClass('has-error');
				$('.withdraw-state-group').removeClass('has-error');	
				$('.withdraw-meat-center-group').removeClass('has-error');		  
				$('.withdraw-service-group').removeClass('has-error');
				$('.withdraw-address-group').removeClass('has-error');

				$('.help-block').remove();
				var form_data = new FormData(this);
                  $.ajax({
                        url: "<?php echo site_url() ?>post_withdraw",
                        method: 'POST',
						data: form_data,
						processData: false,
						contentType: false,
						cache: false,                         
					   dataType: 'json'
                   }).done(function (data){
					if(data.failure){
						$('#withdrawalModal').hide('fast');
                        window.location.replace('<?php echo site_url(); ?>login');
                    }else if(!data.success){
                        if(data.error.withdraw_account_number_phone){
							$('.withdraw-account-number-or-phone-group').addClass('has-error');
							$('.withdraw-account-number-or-phone-group').append('<div class="help-block text-danger" style="font-size:small">' + data.error.withdraw_account_number_phone + '</div>'); // add the actual error message under our input
                        }else{
							if(data.error.withdraw_code){
								$('.withdraw-code-group').addClass('has-error');
								$('.withdraw-code-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.withdraw_code + '</div>'); // add the actual error message under our input
							}else{
								if(data.error.withdraw_size){
									$('.withdraw-kilogram-group').addClass('has-error');
									$('.withdraw-kilogram-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.withdraw_size + '</div>'); // add the actual error message under our input
								}else{
									if(data.error.withdraw_state){
										$('.withdraw-state-group').addClass('has-error');
										$('.withdraw-state-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.withdraw_state + '</div>'); // add the actual error message under our input
									}else{
										if(data.error.withdraw_center){
											$('.withdraw-meat-center-group').addClass('has-error');
											$('.withdraw-meat-center-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.withdraw_center + '</div>'); // add the actual error message under our input
										}else{
											if(data.error.withdraw_service){
												$('.withdraw-service-group').addClass('has-error');
												$('.withdraw-service-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.withdraw_service + '</div>'); // add the actual error message under our input
											}else{
												if(data.error.withdraw_address){
													$('.withdraw-address-group').addClass('has-error');
													$('.withdraw-address-group').append('<div class="help-block error text-danger" style="font-size:small">' + data.error.withdraw_address + '</div>'); // add the actual error message under our input
												}												
											}	
										}
									}									
								}
							}							
                        }
                    }else{
						if(data.response=="withdraw"){
							$('#withdrawModal').hide('fast');
							window.location.replace('<?php echo site_url(); ?>withdraw');
						}else if(data.response=="main"){
							$('#withdrawModal').hide('fast');
							window.location.replace('<?php echo site_url(); ?>/');
						}	
                    }    
          
                 });
            }); 

			$('#logout').on('click', function(){
				$.ajax({
                        url: "<?php echo site_url() ?>logout",
                        method: 'POST',
						data: {logout:"logout"},
						processData: false,
						contentType: false,
						cache: false,                         
					   dataType: 'json'
                   }).done(function (data){	
						if(data.success){
							window.location.replace('<?php echo site_url(); ?>login');
						}
				   });			

			});

			$(".withdraw_by_pickup").on('submit',function(e){
				e.preventDefault(); 
				var form_data = new FormData(this);
                $.ajax({
                     method: "POST",
                     url: "<?php echo site_url(); ?>withdrawal_by_pickup",
					 method: 'POST',
						data: form_data,
						processData: false,
						contentType: false,
						cache: false,                         
					   dataType: 'json'
					}).done(function (data){ 
                    
						if(data.success){
                        	window.location.replace('<?php echo site_url(); ?>/');
						}
                      
                     
                  });
			});				
            
			$("#deposit_state").on('change',function(){
				$state = $(this).val();
                $.ajax({
                     method: "GET",
                     url: "<?php echo site_url(); ?>get_meat_bank_locations/"+$state,
                     dataType: 'json',
                     success: function(data){
						if(data.success){
                        	$('#deposit_center').append(data.locations);
						}else{
							$('#deposit_center').empty();
							$('#deposit_center').append(data.locations);
						}
                      
                     }
                  });
			});			

			$("#withdraw_state").on('change',function(){
				$state = $(this).val();
                $.ajax({
                     method: "GET",
                     url: "<?php echo site_url(); ?>get_meat_bank_locations/"+$state,
                     dataType: 'json',
                     success: function(data){
						if(data.success){
                        	$('#withdraw_center').append(data.locations);
						}else{
							$('#withdraw_center').empty();
							$('#withdraw_center').append(data.locations);
						}
                      
                     }
                  });
			});                                
      });	  

		function load_orders(page) {
			$.ajax({
					method: "GET",
					url: "<?php echo site_url(); ?>get_all_pending_orders_pagination/"+page,
					dataType: 'json',
					success: function(data){
					$('#orders_table').html(data.all_pending_orders_table);
					$('#pagination_link').html(data.pagination_link);
					}
				});
		}
		load_orders(1);
		$(document).on("click", ".pagination li a", function(event){
			event.preventDefault();
			var page = $(this).data("ci-pagination-page");
			load_orders(page);
		});		

		function removeOrder() {

			depositId =$('.deleteOrder').attr('id');
			var result = confirm($('.deleteOrder').attr('data-confirm'));				
			if (result==true) {
				$.ajax({
					method: "GET",
					url: "<?php echo site_url(); ?>delete_deposit_order/"+depositId ,
					dataType: 'json',
					success: function(data){
						if(!data.success){
							window.location.replace('<?php echo site_url(); ?>orders');   
						}else{
							window.location.replace('<?php echo site_url(); ?>orders'); 
						}
					}
				});				
				
			} else {
				return false;
			}

		}
	</script>	
	
  </head>
<body>	