
<div class="main-cover">
    <br>
    <div class="container bootdey">  
        <div class="col-md-12">  
            <div class="col-md-3">      
                <div class="portlet light profile-sidebar-portlet bordered">
                    <div class="profile-userpic">
                        <img src="<?php echo base_url('assets/images/user.jpeg') ?>" class="img-responsive" alt="User Image"> 
                </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> <?php echo $user->name ?> </div>
                        <div class="profile-usertitle-job"> <?php echo $user->account_number ?> </div>
                    </div>
                    <div class="profile-usermenu">
                        <ul class="nav">                        
                            <li class="active">
                                <i class="fa fa-envelope"></i> <?php echo $user->email ?> 
                            </li>
                            <li class="active">
                                <i class="fa fa-phone"></i> <?php echo $user->phone ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9" > 
                <div class="portlet light bordered" style="height:370px;">
                    <div class="row">                   
                        <div class="col-md-12 col-sm-12 logout">
                            <form action="<?php echo site_url('logout')?>">
                                <input type="submit" class="profile-logout-btn"  value="Logout"/>                        
                            </form>                        
                        </div>
                    </div><br>               
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase"><strong>Meat Deposit Order List</strong></span>
                        </div>
                    </div>
                    <div class="portlet-body row table-responsive" id="orders_table">
                        
                    </div>
                    <div id="pagination_link"></div>
                </div>
            </div>
        </div>
    </div>
</div>