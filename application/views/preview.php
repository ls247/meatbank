<div class="main-cover">
    <div class="container">
        <div class="row user-preview">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-2"></div>
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <div class="logo">
                            <a href="<?php echo site_url() ?>"  class="main-logo-a">
                                <img src="<?php echo base_url('assets/images/MeatBank Official Logo.png') ?>"  alt="Logo"  > 
                            </a>
                        </div>              
                    </div>            
                </div>
                <div class="row">
                    <p style="text-align:center"><br> All field mark <i class="text-danger">*</i> are compulsory <br> </p>            
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">                
                        <form method="POST" id="depositPaymentForm" action="" class="" >
                            <script src="https://js.paystack.co/v1/inline.js"></script>
                        
                            <div class="form-group">
                                <input class="form-control" type="hidden" id="d_amount"  name="amount"   value="<?php echo $grand_total ?>" readonly />
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group ">
                                      <label for="Kilogram" class="control-label"> Size Of Meat (KG)</label>
                                      <input type="text"  name="size" class="form-control" value="<?php echo $deposit->kg ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group perUnit-group">
                                       <label for="Meat Type" class="control-label"> Meat Deposited</label>
                                       <input type="text"   name="meat-type" value="<?php echo ucfirst($meat->meat_type) ?>" class="form-control" readonly>                        
                                    </div>																
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label  for="amount">Storage Charge</label>
                                        <input class="form-control" type="text"  name="amount" value="&#8358;<?php echo $deposit->amount ?>" readonly />
                                    </div>                                    
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label  for="amount">Meat Center</label>
                                        <input class="form-control" type="text"  name="center" value="<?php echo $meat_bank_center?>" readonly />
                                    </div>                                    
                                </div>                                
                            </div>  
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label  for="days">Duration</label>
                                        <input class="form-control" type="text"  name="days" value="<?php echo $duration ?>" readonly />
                                    </div>                                    
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label  for="delivery fee">Delivery Fee</label>
                                        <input class="form-control" type="text"  name="center" value="&#8358;<?php echo $delivery_fee?>" readonly />
                                    </div>                                    
                                </div>                                
                            </div>  
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input class="form-control" type="text" id="name" name="name" value="<?php echo $user->name ?>" readonly>
                                    </div>                                   
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label  for="email">Email</label>
                                        <input class="form-control" type="email" id="d_email" name="email" value="<?php echo $user->email ?>" placeholder="Enter A Valid Email Address" readonly>
                                    </div>                                   
                                </div>                                
                            </div>                                                                                 
                            <div class="form-group">
                                <label for="Phone Number">Phone Number </label><i class="text-danger">*</i>
                                <input class="form-control" type="number" id="d_phone_number" name="phone_number" value="<?php echo $user->phone ?>"  placeholder="Phone Number.." required >
                            </div>
                            <div class="form-group">
                                <label  for="delivery_address">Delivery Address </label><i class="text-danger">*</i>
                                <textarea class="form-control" type="text" id="d_delivery_address"  name="delivery_address"  placeholder="Delivery Address.." required ><?php echo $deposit->address ?></textarea>
                            </div>
                            <div class="form-group">
                                <button  class="btn btn-info"  type="" onclick="depositPayWithPaystack();" name="save" value="Pay" id="saver">Make Payment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div> 
<script>
        var depositPaymentForm = document.getElementById('depositPaymentForm');
        depositPaymentForm.addEventListener('submit', depositPayWithPaystack, false);
        function depositPayWithPaystack(e){
			e.preventDefault();	
            var handler = PaystackPop.setup({
                key: 'pk_test_bc9a0d7fb2c4c13c4fc1d9f65d44da3cc55d7f45',
                // key: 'pk_live_b17badea73dd7d69efa07dbb9c8edf1d36256821',
                email:  document.getElementById("d_email").value,
                amount: document.getElementById("d_amount").value * 100, // the amount value is multiplied by 100 to convert to the lowest currency unit
                currency: 'NGN', // Use GHS for Ghana Cedis or USD for US Dollars
                reference: '<?php echo generateReferenceNumber(8) ?>', // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                metadata: {
                    custom_fields: [
                        {
                            display_name: "Mobile Number",
                            variable_name: "mobile_number",
                            mobile_number: document.getElementById("d_phone_number").value,
							address: document.getElementById("d_delivery_address").value
							
                        }
                    ]

                },
                callback: function(response){
                    
                    //this happens after the payment is completed successfully
                    window.location.href = "http://localhost/meatbank/verify_deposit_transaction/"+response.reference;
                    
                },
                onClose: function(){
                    alert('Transaction was not completed, window closed.');
					window.location = "http://localhost/meatbank/";
                }
            });
            handler.openIframe();

        }

</script> 