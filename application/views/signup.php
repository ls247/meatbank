<div class="container">
        <div class="col-md-2"></div>
          <div class="col-md-8" style="margin-top:30px">
            <?php
              if( !empty($this->session->flashdata('message_success'))){ ?>
                        <div class=' alert alert-info alert-dismissable ' style='text-align:center;font-size:small;'>
                                <a href='#' class='close' data-dismiss='alert' aria-label='close' style='margin-right:50px'>&times</a>
                                <?php echo $this->session->flashdata('message_success'); ?>
                        </div>
              <?php }else if(!empty($this->session->flashdata('message_danger'))) { ?>
                        <div class=' alert alert-danger alert-dismissable ' style='text-align:center;font-size:small;'>
                                <a href='#' class='close' data-dismiss='alert' aria-label='close' style='margin-right:50px'>&times</a>
                                <?php echo $this->session->flashdata('message_danger'); ?>
                        </div>
              <?php } ?>              
          </div>
          <div class="col-md-2"></div>    
         <div class="col-lg-4 col-md-3 col-sm-2"></div>
         <div class="col-lg-4 col-md-6 col-sm-8">
                 
                <div class="logo">
                        <a href="<?php echo site_url() ?>"  class="main-logo-a">
                                <img src="<?php echo base_url('assets/images/MeatBank Official Logo.png') ?>"  alt="Logo"  > 
                        </a>
                </div>
                <div class="row signupbox">
                           <form action='<?php echo site_url('signup') ?>' method="POST">
                                    <div class="col-lg-12">
                                             <span class="singtext" >SignUp</span>   
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                             <input class="form-control" type="text" name="fullname" placeholder="Please enter your fullname">
                                             <?php echo form_error('fullname') ?>
                                    </div>
                                    <div class="col-lg-12  col-md-12 col-sm-12">
                                             <input class="form-control" type="text" name="email" placeholder="Please enter your email">
                                             <?php echo form_error('email') ?>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                             <input class="form-control" type="text" name="phoneNumber" placeholder="Please enter your phone number">
                                             <?php echo form_error('phoneNumber') ?>
                                    </div>
                                    <div class="col-lg-12  col-md-12 col-sm-12">
                                             <input class="form-control" type="password" name="password" placeholder="Please enter your password">
                                             <?php echo form_error('password') ?>
                                    </div>                    
                                    <div class="col-lg-12  col-md-12 col-sm-12">
                                             <button type="submit" class="btn submitButton">Submit </button> 
                                    </div>                     
                           </form>
                  </div>
                  <div class="row forGotPassword">
                           <p><a href="<?php echo site_url('login') ?>">Login Here</a></p>                                            
                           <a href="<?php echo site_url('forgot-password') ?>" > Forgot Password? </a>
                  </div>
                  <br>                
            </div>
            <div class="col-lg-4 col-md-3 col-sm-2"></div>
</div>