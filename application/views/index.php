<div class="container-fluid cover-extention-1">
    <div class="row">
      <div class="main-logo">
        <a href="<?php echo site_url() ?>" class="main-logo-a">
         <img src="<?php echo base_url('assets/images/MeatBank Official Logo.png') ?>">  
        </a>
           
      </div>
      <div class="nav-menu">
        <ul>
          <?php 
            if($this->session->userdata('logged_in_id')!=""){
          ?>
            <li class="fullname-bage">
              <?php echo $this->session->userdata('fullname') ?>
              &nbsp;
              &nbsp;
              <i class="fa fa-sign-out" title="Logout" id="logout"> </i> 
            </li>
          <?php
            }else{
          ?> 
            <li class="btn-primary primary-extra"> <a href="<?php echo site_url('login') ?>" > Login</a></li>
            <li class="btn-outline-primary primary-extra"> <a href="<?php echo site_url('signup') ?>" > Signup</a></li>              
          <?php
            }
          ?>        

        </ul>
      </div>         
    </div>
    <div class="main-parag">
      <div class="row">
        <div class="col-md-6 col-sm-12 main-heading">
          <h1 id="main-heading"></h1>
        </div>
        <div class="col-md-6 col-sm-12 main-frame">
          <img src="<?php echo base_url('assets/images/Red.png') ?>">
        </div>          
      </div>
    </div>
  </div>
  <section>
    <div class="cover-extention-2">
      <div class="row">
        <div class="main-icon">
            <img src="<?php echo base_url('assets/images/wallet.png') ?>">
            <p>Deposit your meat to our nearest meat bank location</p>               
            <a class="btn btn-info" data-title="Deposit" data-toggle="modal" data-target="#depositModal" >Deposit</a>        
        </div>
        <div class="main-icon">
            <img src="<?php echo base_url('assets/images/withdraw.png') ?>">
            <p>Widthdrawal your meat either online or offline</p> 
            <a class="btn btn-info" data-title="Withdrawal" data-toggle="modal" data-target="#withdrawalModal">Withdrawal</a>
        </div>
        <div class="main-icon">
            <img src="<?php echo base_url('assets/images/transfer.png') ?>" style="color:white;height:80px">
            <p>Transfer meat to any meat bank account</p> 
            <a class="btn btn-info">Coming soon</a>
        </div>
      </div>         
    </div>
  </section>
   <!-- modal for Depositing meat -->
  <div class="modal col-md-12 col-sm-12" id="depositModal" tabindex="-1" role="dialog" aria-labelledby="deposit" aria-hidden="true" >                  
    <div class="modal-dialog modal-size">
      <div class="modal-content">
        <div class="modal-header deposit-total">
        <h4 class="modal-title text-info text-center" id="Heading" style="font-size:30px">Meat Deposit Box </h4>
        </div>
        <br>                        
        <form method="post" id="deposit">
          <div class="modal-body">
            <div class="row" id="deposit-block">

            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="form-group accountNumber-group">
                  <label for="Account Number" class="control-label"> Account Number</label>
                    <input type="text" id="account_number"  name="account_number" class="form-control" placeholder="Enter your meat bank account number">
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="form-group meat-type-group">
                  <label for="Meat Type" class="control-label">Meat Type</label>
                  <select id="meat_type" name="meat_type" class="form-control">
                      <?php if(isset($meat_types) && $meat_types !=false) {?>
                          <option value="none">Select-Meat-Type </option>                   
                            <?php foreach($meat_types as $meat){ ?>
                                <option value="<?php echo $meat->id ?>"><?php echo ucfirst($meat->meat_type) ?></option> 
                            <?php } ?>
                      <?php }else{ ?>
                          <option value="none">Select-Meat-Type </option> 
                      <?php } ?>
                  </select>
                </div>
              </div>              
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group kilogram-group" id="kilo">
                      <label for="Kilogram" class="control-label"> Kilogram (KG)</label>
                      <input type="text"  id="size" name="size" value="1" class="form-control" placeholder="Please provide your meat kilogram" onkeyup="covertKilo()">
                      <div id="size-group"></div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group perUnit-group">
                      <label for="Price Per KG" class="control-label"> Price Per KG (&#8358)</label>
                      <input type="text"  id="perUnit" name="perUnit" value="50" class="form-control" readonly>                        
                    </div>																
                  </div>
                </div>
              </div> 
              <div class="col-md-6 col-sm-6">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group days-group" id="day">
                      <label for="Days" class="control-label">Number Of Days</label>
                      <input type="text"  id="days" name="days" value="1" class="form-control" onkeyup="covertDays()">
                      <div id="days-group"></div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group total-group">
                      <label for="Total" class="control-label">Total (&#8358)</label>															
                      <input type="text"  id="total" name="total" value="50" class="form-control" readonly>					                        
                    </div>											
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="form-group service-group">
                  <label for="Service" class="control-label">Service (Pick Up/Drop Off)</label>
                  <select id="service" name="service" class="form-control">
                    <option value="none">Select-Service (Pick Up/Drop Off)</option>
                    <option value="pickup">Pick Up</option>
                    <option value="dropoff">Drop Off</option>
                  </select>
                </div>
              </div>                
              <div class="col-md-6 col-sm-6">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group deposit-state-group">
                      <label for="state" class="control-label"> State</label>
                      <select class="form-control" id="deposit_state" name="state">
                        <?php if(isset($states) && $states !=false) {?>
                          <option value="none">Select-State</option>                   
                              <?php foreach($states as $state){ ?>
                                <?php if( in_array($state->state_name, array("Lagos State", "Edo State"))) { ?>
                                  <option value="<?php echo $state->id ?>"><?php echo ucfirst($state->state_name) ?></option> 
                                <?php } ?>  
                              <?php } ?>
                        <?php }else{ ?>
                          <option value="none">Select-Meat-Type </option> 
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group deposit-meat-center-group">
                      <label for="Meat Bank Center" class="control-label">Meat Bank Center</label>
                      <select id="deposit_center" name="center" class="form-control">
                        <option value="none">Select-Meat-Bank-Center</option>
                      </select>
                      <div id="center-group"></div>
                    </div>
                  </div> 
                </div>                  
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" id="addAddress">
                <div class="form-group address-group">
                  <label for="Address" class="control-label">Address</label>
                  <textarea  id="address" name="address" placeholder="Enter your pickup address location" class="form-control"></textarea>
                  <div id="address-group"></div>
                </div>
              </div>               
            </div>              
          </div>  
          <div class="modal-footer">
            <div class="row">
              <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6">
                <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save</button>                
              </div>
              <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-remove"></span> Cancel</button>                
              </div>              
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


 <!-- modal for withdrawing meat -->
  <div class="modal col-md-12 col-sm-12" id="withdrawalModal" tabindex="-1" role="dialog" aria-labelledby="withdrawal" aria-hidden="true" >                  
    <div class="modal-dialog modal-size">
      <div class="modal-content">
        <div class="modal-header deposit-total">
        <h4 class="modal-title text-info text-center" id="Heading" style="font-size:30px">Meat Withdrawal Box</h4>
        </div>
        <br>
        <div class="row" id="withdrawal-details">
          <div class="col-md-12" >  
            <div class="col-md-4">      
              <div class="portlet light profile-sidebar-portlet bordered">
                <div class="profile-userpic">
                  <img src="<?php echo base_url('assets/images/user.jpeg') ?>" class="img-responsive" alt=""> 
                </div>
                <div class="profile-usertitle">
                  <div class="profile-usertitle-name"> </div>
                  <div class="profile-usertitle-job"></div>
                </div>
                <div class="profile-usermenu">
                  <ul class="nav">                        
                    <li class="active profile-usertitle-email"> 
                    </li>
                    <li class="active profile-usertitle-phone">
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-8" > 
              <div class="portlet light bordered" style="height:300px;">              
                <div class="portlet-title tabbable-line">
                  <div class="caption caption-md">
                      <i class="icon-globe theme-font hide"></i>
                      <span class="caption-subject font-blue-madison bold uppercase"><strong>Withdrawal Details</strong></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 deposited_kg"></div>
                  <div class="col-md-6 kg_withdrawn"></div>
                </div><br>
                <div class="row" id="total_kg_left">
                  <div class="col-md-6 total_kg_left"></div>
                  <div class="col-md-6 service"></div>                    
                </div><br>
                <div class="row">
                  <div class="col-md-6 meat_bank_location"></div>
                  <div class="col-md-6 delivery_charge"></div>              
                </div><br>
                <div class="row withdrawal_address">
                  <div class="col-md-12 delivery_address"></div>               
                </div>                
              </div>
            </div><br>
            <div class="col-md-8">
              <form method="POST" id="withdrawalPaymentForm" action="">
              <script src="https://js.paystack.co/v1/inline.js"></script>
                <input type="hidden" id="w_email" name="email">
                <input type="hidden" id="w_amount" name="amount">
                <input type="hidden" id="w_phone_number" name="phone_number" >
                <input type="hidden" id="w_delivery_address">
                <div class="row" id="no-deposit">
                  <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6" id="payment-button">
                      <button  class="btn btn-primary"  type="" onclick="withdrawalPayWithPaystack();" name="save" value="Pay" id="saver"><span class="fa fa-check-circle"></span>  Make Payment</button>
                  </div>
                  <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6" id="withdrawal-button">
                    <button type="button"  class="btn btn-primary withdrawal-button"><span class="fa fa-check-circle"></span> Proceed</button>                
                  </div>
                  <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-remove"></span> Cancel</button>                
                  </div>                                 
                </div>                                             
              </form>
            </div>              
          </div>
        </div>                

      
        <form method="post" id="withdraw">
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-md-12">
              <label for="Note" class="control-label">Note:</label> 
              <i>Dear Customer you can only withdraw a minimum of 1 (kg)</i>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="form-group withdraw-account-number-or-phone-group">
                  <label for="Account Number" class="control-label"> Account Number / Phone Number</label>
                  <input type="text" id="withdraw_account_number_or_phone"  name="withdraw_account_number_or_phone" class="form-control" placeholder="Enter your meat bank account number or phone number">
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group withdraw-code-group">
                      <label for="Withdrawal Code" class="control-label"> Withdrawal Code</label>
                      <input type="text"  id="withdraw_code" name="withdraw_code"  class="form-control" Placeholder="Enter your withdrawal code">                        
                    </div>																
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group withdraw-kilogram-group">
                      <label for="Kilogram" class="control-label"> Kilogram (KG)</label>
                      <input type="text"  id="size" name="withdraw_size" value="1" class="form-control" placeholder="Please provide your meat kilogram">
                    </div>
                  </div>                    
                </div>
              </div>              
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group withdraw-state-group">
                      <label for="state" class="control-label"> State</label>
                      <select class="form-control" id="withdraw_state" name="state">
                        <?php if(isset($states) && $states !=false) {?>
                          <option value="none">Select-State</option>                   
                            <?php foreach($states as $state){ ?>
                              <?php if( in_array($state->state_name, array("Lagos State", "Edo State"))) { ?>
                                <option value="<?php echo $state->id ?>"><?php echo ucfirst($state->state_name) ?></option> 
                              <?php } ?>   
                            <?php } ?>
                        <?php }else{ ?>
                          <option value="none">Select-Meat-Type </option> 
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group withdraw-meat-center-group">
                      <label for="Meat Bank Center" class="control-label">Meat Bank Center</label>
                      <select id="withdraw_center" name="withdraw_center" class="form-control">
                        <option value="none">Select-Meat-Bank-Center</option>
                      </select>
                      <div id="center-group"></div>
                    </div>
                  </div> 
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group withdraw-service-group">
                      <label for="Service" class="control-label">Service (Pick Up/Drop Off)</label>
                      <select id="withdraw_service" name="withdraw_service" class="form-control">
                        <option value="none">Select-Service (Pick Up/Drop Off)</option>
                        <option value="pickup">Pick Up</option>
                        <option value="dropoff">Drop Off</option>
                      </select>
                    </div>
                  </div> 
                  <div class="col-md-6 col-sm-6" id="optional_address">
                    <div class="form-group checkbox-group">
                      <label for="Optional Instruction" class="control-label" > Would you like to change your address location</label>
                      <input type="checkbox" name="address_change" id="address_change" class="control-checkbox" value="address">
                    </div>
                  </div> 
                </div>
              </div>                                      
            </div>
            <div class="row" id="add-address">
              <div class="col-md-12 col-sm-12">
                <div class="form-group withdraw-address-group">
                  <label for="address" class="control-label">Delivery Address</label>
                  <textarea class="form-control" id="withdrawal-address" name="withdrawal_address" placeholder=""></textarea>
                </div>
              </div>
            </div>            
          </div>
          <div class="modal-footer">
            <div class="row">
              <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6">
                <button type="submit"  class="btn btn-primary"><span class="fa fa-check-circle"></span> Withdraw</button>                
              </div>
              <div class="col-md-5 offset-md-1 col-sm-12 col-xs-6 col-xxs-6">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-remove"></span> Cancel</button>                
              </div>              
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

