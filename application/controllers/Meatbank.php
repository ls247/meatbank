<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meatbank extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {      
        parent::__construct();
		$this->load->helper(array('form', 'url', 'generate_account_number'));
		$this->load->library('form_validation');
		$this->load->database();
        $this->load->model('meatbank_model');
    }
    public function index(){
		$this->Meatbank_model->createSuperAdmin();
		$this->Meatbank_model->createState();
		$states = $this->Meatbank_model->get_states();
		$meat_types = $this->Meatbank_model->get_meat_types();
		$this->load->view('includes/head');
		$this->load->view('index.php',["states"=>$states, "meat_types"=>$meat_types] );
		$this->load->view('includes/footer');


    }
    
	public function post_deposit(){
		if($this->input->server('REQUEST_METHOD') === 'POST'){
		
			$account_number = $this->input->post('account_number');
			$size = (int)$this->input->post('size');
			$price_per_unit =  (int)$this->input->post('perUnit');
			$days = (int)$this->input->post('days');
			$total = (int)$this->input->post('total');
			$meat_type = $this->input->post('meat_type');
			$service = $this->input->post('service');
			$state = $this->input->post('state');
			$center = $this->input->post('center');
			$address = $this->input->post('address');
			$data = array();
			$error = array();
			
			if(isset($account_number) && empty($account_number)){ 
				$error['account_number'] ='Meat bank account number is required';
			}else{
				if($this->Meatbank_model->check_account_number($account_number) !=1){
					$error['account_number'] ='Kindly put your correct meat bank account number';			
				}else{
					if(isset($size) && empty($size) || $size == 0 || is_nan($size)){
						$error['size'] ='Meat size is needed';
					}else{
						if(isset($per_unit_price) && empty($per_unit_price)){
							$per_unit_price = 50;
						}else{
							if(isset($days) && empty($days) || $days == 0 || is_nan($days)){
								$error['days'] ='Please specify number of days';
							}else{
								if(isset($meat_type) && empty($meat_type) || $meat_type == "none"){
									$error['meat_type'] ='Please select your meat type';
								}else{
									if(isset($service) && empty($service) || $service == "none"){
										$error['service'] ='Please select your preferred service';
									}else{
										if(isset($state) && empty($state) || $state == "none"){
											$error['state'] ='Please select your state';
										}else{
											if(isset($center) && empty($center) || $center == "none"){
												$error['center'] ='Please select the center you prefer';
											}else{
												if($service=="pickup"){
													if(isset($address) && empty($address)){
														$error['address'] ='Please provide your address location';
													}
												}

											}
										}										

									}
								}								
							}								
						}						
					}						
				}
			}
																					
			if(empty($error)){
				$this->session->set_userdata('account_number', $account_number);
				$this->session->set_userdata('size', $size);
				$this->session->set_userdata('price_per_unit', $price_per_unit);
				$this->session->set_userdata('days', $days);
				$this->session->set_userdata('total', $total);
				$this->session->set_userdata('meat_type', $meat_type);
				$this->session->set_userdata('service', $service);				
				$this->session->set_userdata('state', $state);
				$this->session->set_userdata('center', $center);
				$this->session->set_userdata('address', $address);
				$bag_code_id = $this->Meatbank_model->get_random_bag_code($center);	
				if($bag_code_id ==''){
					$data['failure'] = true;
					$data['response'] = 'locked';
				}
				$meat = $this->Meatbank_model->get_meat($meat_type);
				$this->session->set_userdata('meat', $meat);
				$this->session->set_userdata('bag_code_id', $bag_code_id);				
				if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user" && $this->session->userdata('account_number')){
					$user_id = $this->session->userdata('logged_in_id');
					$user = $this->Meatbank_model->get_user($user_id);
					$user_id = $user->id;
					$status = "pending";
					if($this->Meatbank_model->meat_deposit($user_id, $size, $total, $meat_type, $center, $bag_code_id, $days, $service, $status, $address) == 1){
						$data['response'] = 'deposit';					
						$data['success'] = true;
					}										
				}elseif($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user" ){
					$data['response'] = 'main';					
					$data['success'] = true;	
				}
				else{
					$this->session->set_flashdata('message_danger', '<strong>Kindly login first to deposit your meat<strong>');
					$data['failure'] = true;
				}
			}else{
				$data['error']=$error;
				$data['success'] = false;
			}
			echo json_encode($data);
		}		
	}
	public function post_withdraw(){
		if($this->input->server('REQUEST_METHOD') === 'POST'){
		
			$account_number_or_phone = $this->input->post('withdraw_account_number_or_phone');
			$size = (int)$this->input->post('withdraw_size');
			$withdrawal_code = $this->input->post('withdraw_code');
			$service = $this->input->post('withdraw_service');
			$state = $this->input->post('withdraw_state');
			$center = $this->input->post('withdraw_center');
			$addressChecked = $this->input->post('address_change');
			$address = $this->input->post('withdrawal_address');
			$data = array();
			$error = array();
			
			if(isset($account_number_or_phone) && empty($account_number_or_phone)){ 
				$error['withdraw_account_number_phone'] ='Meat bank account number or phone is required';
			}else{
				if($this->Meatbank_model->check_account_number_phone($account_number_or_phone) !=1){
					$error['withdraw_account_number_phone'] ='Kindly put your correct meat bank account number or phone number';			
				}else{
					if(isset($size) && empty($size) || $size == 0 || is_nan($size)){
						$error['withdraw_size'] ='Meat size is needed';
					}else{
						
						if($this->Meatbank_model->check_available_kg($withdrawal_code) < $size){
							$error['withdraw_size'] ='You do not have any meat stored in our meatbank';
						}else{
							if(isset($withdrawal_code) && empty($withdrawal_code)){
								$error['withdraw_code'] ='Please type in your withdrawal code';							
							}else{
								if($this->Meatbank_model->check_withdrawal_code($withdrawal_code) !=1){
									$error['withdraw_code'] ='Kindly put your correct withdrawal code';			
								}else{
									if(isset($state) && empty($state) || $state == "none"){
										$error['withdraw_state'] ='Please select your state';
									}else{
										if(isset($center) && empty($center) || $center == "none"){
											$error['withdraw_center'] ='Please select the center you prefer';
										}else{
											if(isset($service) && empty($service) || $service == "none"){
												$error['withdraw_service'] ='Please select your preferred service';
											}else{
												if(isset($addressChecked) && $addressChecked == "address" && isset($address) && empty($address)){
													$error['withdraw_address'] ='Please provide your delivery address';
												}
											}
										}
									}								
								}							
							}							
						}
					}						
				}
			}
														
												
			if(empty($error)){

				$this->session->set_userdata('account_number_or_phone', $account_number_or_phone);
				$this->session->set_userdata('withdrawal_code', $withdrawal_code);
				$this->session->set_userdata('kg_withdrawn', $size);
				$this->session->set_userdata('service', $service);
				$this->session->set_userdata('center', $center);
				$location_center_details = $this->Meatbank_model->get_location_details($center);
				$this->session->set_userdata('meat_bank_location', $location_center_details->location_name);
				if(isset($addressChecked) && $addressChecked == "address"){
					$this->session->set_userdata('address', $address);
				}
				if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') 
					&& $this->session->userdata('role_logged_on') == "user" 
					&& $this->session->userdata('account_number_or_phone') && $this->session->userdata('withdrawal_code') ){
				    $data['response'] = 'withdraw';	
					$data['success'] = true;									
				}elseif($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user" ){
					$data['response'] = 'main';					
					$data['success'] = true;	
				}else{
					$this->session->set_flashdata('message_danger', '<strong>Kindly login first to withdraw your meat<strong>');
					$data['failure'] = true;
				}
			}else{
				$data['error']=$error;
				$data['success'] = false;
			}
			echo json_encode($data);
		}
	}
	public function withdrawal_by_pickup(){

        $data = array();
		$withdrawal_code = $this->session->userdata('withdrawal_code');
		$kg_withdrawn = $this->session->userdata('kg_withdrawn');
		$meat_id = $this->session->userdata('meat_id');
		$meat_type = $this->session->userdata('meat_type');
		$meat_bank_location = $this->session->userdata('meat_bank_location');
		$deposit_id = $this->session->userdata('deposited_id');
		$service = $this->session->userdata('service');
		$address = $this->session->userdata('address');
		$deposited_kg = $this->session->userdata('deposited_kg');
		$total_kg_left = $this->session->userdata('total_kg_left');
		$transaction_time = date('h:i:s a');
		$status = '';
		$paystack_ref = '';
		$amount_paid = '';
		$address = 
		$user_id = $this->session->userdata('logged_in_id');
		$user = $this->Meatbank_model->get_user($user_id);
		$this->Meatbank_model->insert_withdrawal($user->id, $deposit_id, $paystack_ref,  $transaction_time, $kg_withdrawn, $status, $amount_paid);
		$this->Meatbank_model->update_deposit_withdrawal($user->id, $withdrawal_code, $total_kg_left, $transaction_time, $address);
		$user_id = $user->id;
		$fullname = $user->name;
		$email = $user->email;
		$phone_number =$user->phone;
		$ci = get_instance();
		$ci->load->library('email');
		$config = array('protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => '465', // 465 587
			'smtp_user' => 'admin@livestock247.com',
			'smtp_pass' => 'b861N8cPchr3',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',                      
			'wordwrap' => TRUE,
		);
		$ci->email->initialize($config);
		$ci->email->set_newline("\r\n");                        
		$ci->email->from('admin@livestock247.com', 'Admin');
		$ci->email->to($email);
		$ci->email->subject('Hello '.ucwords($fullname).' '.$kg_withdrawn.'kg Worth Of '.ucfirst($meat_type).' Has Been Withdrawn');
		$mail_message='<div style="width:100%;padding-bottom:20px;">
							<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
								<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
								<div style="width:100%;margin:30px">
								<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
								<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
								</div>
							</div>
							<div style="margin-top:20px;width:100%">
								<p>Dear '.ucwords($fullname).' '.$kg_withdrawn.'kg worth of '.$meat_type.' has been withdrawn from your'.$deposited_kg.'kg worth of '.$meat_type.' to be picked up from our meat bank center '.$meat_bank_location.'</p>
							</div>
						</div>';
		$ci->email->message($mail_message);
		if($ci->email->send()){
			$this->Meatbank_model->send_sms_withdraw($user_id, $fullname, $phone_number, $kg_withdrawn, $total_kg_left, $meat_type, $amount_paid, $meat_bank_location);
			$ci = get_instance();
			$ci->load->library('email');
			$config = array('protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => '465', // 465 587
				'smtp_user' => 'admin@livestock247.com',
				'smtp_pass' => 'b861N8cPchr3',
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',                      
				'wordwrap' => TRUE,
			);
			$ci->email->initialize($config);
			$ci->email->set_newline("\r\n");                        
			$ci->email->from('admin@livestock247.com', 'Admin');
			$ci->email->to('methyl2007@yahoo.com');
			$ci->email->subject('Meat Got Withdrawn');
			$mail_message='<div style="width:100%;padding-bottom:20px;">
								<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
									<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
									<div style="width:100%;margin:30px">
									<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
									<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
									</div>
								</div>
								<div style="margin-top:20px;width:100%">
									<p>Hello team, customer with the Name: '.ucwords($fullname).' and Phone: '.$phone_number.' is withdrawing '.$kg_withdrawn.'kg worth of '.$meat_type.' from his or her '.$deposited_kg.'kg worth of '.$meat_type.'</p>
									<p>Our meat bank center '.$meat_bank_location.' or any of our available meat bank center </p>';
								'</div>
							</div>';
			$ci->email->message($mail_message);
			if($ci->email->send()){
				redirect('/');
			}
		}
		//echo json_encode($data);
	}
    public function login()
	{
		if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user"){
           redirect('/');
		}

        if($this->input->server('REQUEST_METHOD') === 'POST'){
            $this->form_validation->set_rules('username', 'Username', 'trim|required');            
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');          
            $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
			$this->form_validation->set_message('required', ' %s is required');
            if ($this->form_validation->run() == FALSE){
				$this->Meatbank_model->createSuperAdmin();					
				$this->load->view('includes/head');
				$this->load->view('login.php' );
				$this->load->view('includes/footer');
            }else{
                $username=$this->input->post('username');
                $password= $this->input->post('password');
                if($this->Meatbank_model->login_account($username, $password) == 1){
					$result = $this->Meatbank_model->user_information($username);
					$role = $this->Meatbank_model->get_role($result[0]->role_id);
                    $this->session->set_userdata('logged_in_id', $result[0]->id);	
					$this->session->set_userdata('fullname', $result[0]->name);				
                    $this->session->set_userdata('role_logged_on', $role);				
					if($this->session->userdata('account_number') && $this->session->userdata('bag_code_id')){
						$size = $this->session->userdata('size');
						$total = $this->session->userdata('total');
						$center = $this->session->userdata('center');
						$meat_type = $this->session->userdata('meat_type');
						$bag_code_id = $this->session->userdata('bag_code_id');
						$days = $this->session->userdata('days');
						$service = $this->session->userdata('service');
						$address = $this->session->userdata('address');
						$user_id = $this->session->userdata('logged_in_id');
						$user = $this->Meatbank_model->get_user($user_id);
						$user_id = $user->id;
						$status = "pending";
						$this->Meatbank_model->meat_deposit($user_id, $size, $total, $meat_type, $center, $bag_code_id, $days, $service, $status, $address);							
						redirect('deposit');
					}elseif($this->session->userdata('account_number_or_phone') && $this->session->userdata('withdrawal_code')){
						redirect('withdraw');
					}else{
						redirect('/');
					}

                }else{
					$this->session->set_flashdata('message_danger', '<strong> Kindly put the right login details</strong>');            
					redirect('login');
                } 
            }            
        }else{
			$this->Meatbank_model->createSuperAdmin();					
			$this->load->view('includes/head');
			$this->load->view('login.php' );
			$this->load->view('includes/footer');           
        } 		
    }

    public function signup(){
		if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user"){
			redirect('/');
		 }		
        if($this->input->server('REQUEST_METHOD') === 'POST'){
            $this->form_validation->set_rules('fullname', 'Fullname', 'trim|required');            
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phoneNumber', 'Phone number', 'trim|required|regex_match[/^[0-9]{11}$/]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|alpha_numeric');          
            $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
			$this->form_validation->set_message('required', ' %s is required');
            if ($this->form_validation->run() == FALSE){
                $this->load->view('includes/head');
				$this->load->view('signup.php' );
				$this->load->view('includes/footer');
            }else{
                $fullname=$this->input->post('fullname');
                $email=$this->input->post('email');
                $phone_number=$this->input->post('phoneNumber');
                $password= hashPassword($this->input->post('password'));
                $account_number = generateAccountNumber(6);
				$status = 'unverified';
				$role = 3;
                if($this->Meatbank_model->signup($fullname, $email, $phone_number, $account_number, $password, $status, $role) == 1){
					$qstring= generateToken(8);
					$id = $this->db->insert_id();
					$this->Meatbank_model->update_token($qstring, $id);
					$link ='<a href="'.site_url()."verify/".$qstring.'">'.site_url()."verify/".$qstring.'</a>';                					
                    $ci = get_instance();
                    $ci->load->library('email');
                    $config = array('protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.gmail.com',
                         'smtp_port' => '465', // 465 587
                         'smtp_user' => 'admin@livestock247.com',
                         'smtp_pass' => 'b861N8cPchr3',
                         'mailtype' => 'html',
                         'charset' => 'iso-8859-1',                      
                         'wordwrap' => TRUE,
                       );
                    $ci->email->initialize($config);
                    $ci->email->set_newline("\r\n");                        
                    $ci->email->from('admin@livestock247.com', 'Admin');
                    $ci->email->to($email);
                    $ci->email->subject('Welcome To Livestock247 MeatBank Platform');
                    $mail_message='<p>Dear '.ucwords($fullname).', your account has been created successfully</p>'. "\r\n";
					$mail_message.='<p>Use the information below.</p>';
					$mail_message.='<p>You can either login with your account number: '.$account_number.', email: '.$email.', phone number: '.$phone_number.'</p>';
					$mail_message.='<p><h3 class="btn btn-primary"><a href="'.site_url()."verify/".$qstring.'" class="btn btn-primary btn-md btn-block">Verify Account </a></h3><p>';
					$mail_message.='<p> OR copy and paste the below link in your browser <br> "'.$link.'"</p>';
                    $ci->email->message($mail_message);
                    if($ci->email->send()){
                        $this->session->set_flashdata('message_success', '<strong>'.ucwords($fullname).' you have successfully created an account with '.$email.'</strong> on meatbank, kindly check your mail to verify your account');              
                        redirect('signup');
                    }else{
						$this->Meatbank_model->delete_account($id);
                        $this->session->set_flashdata('message_danger', '<strong> Ooop!!! something went wrong, try again later </strong>');            
                        redirect('signup');
                    } 
                }else{
                        $this->session->set_flashdata('message_danger', '<strong>You already have an account with email '.$email.', kindly go to login </strong>');            
                        redirect('signup');
                } 
            }            
        }else{
			$this->Meatbank_model->createSuperAdmin();		
			$this->load->view('includes/head');
			$this->load->view('signup.php' );
			$this->load->view('includes/footer');           
        } 		
    }

    public function verify(){
		$qstring = $this->uri->segment(3);
		$status = "verified";
        if($this->Meatbank_model->verify($qstring, $status) == 1){
			$this->session->set_flashdata('message_success', '<strong> You have successfully verified your account, you can now login . </strong>');            
			redirect('login');                        
        }else{
			$this->session->set_flashdata('message_danger', '<strong> You don\'t have the right access.</strong>');            
			redirect('login');                        
        }
	}
	
	public function verify_deposit_transaction(){

		$reference = $this->uri->segment(2);
		if($reference == ""){
			header("Location:javascript://history.go(-1)");
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.paystack.co/transaction/verify/".rawurlencode($reference),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer sk_test_41c1144e4ddf66786d22a25af1aed53cf8712ee1", //replace this with your own test key
		"Cache-control: no-cache",
		),
		));
	
		$response = curl_exec($curl);

		$err = curl_error($curl);
		$res = json_decode($response, true);
		if($err){
		// there was an error contacting the Paystack API
		die('Curl returned error: ' . $err);
		}else{
			if($res["data"]["status"]=="success"){			
				$paystack_ref = $res["data"]["reference"];
				$withdrawal_code = generateWithdrawalCode(6);
				$transaction_time = date('h:i:s a');
				$status = 'paid';
				$address = $res["data"]["metadata"]["custom_fields"][0]["address"];
				$phone_number = $res["data"]["metadata"]["custom_fields"][0]["mobile_number"];
				$user_id = $this->session->userdata('logged_in_id');
				$user = $this->Meatbank_model->get_user($user_id);
				$bag_code_id = $this->session->userdata('bag_code_id');
				$this->Meatbank_model->update_deposit($user->id, $bag_code_id, $paystack_ref, $withdrawal_code, $transaction_time, $status, $address);
				$this->Meatbank_model->update_user($user->id, $phone_number);
				$user_id = $user->id;
				$fullname = $user->name;
				$email = $user->email;
				$deposit = $this->Meatbank_model->get_deposit($user->id, $bag_code_id);
				$size = $deposit->kg;
				$service = $deposit->service;
				$amount = $deposit->amount;
				$meat = $this->Meatbank_model->get_meat($deposit->meat_type_id);
				$bag_code = $this->Meatbank_model->get_bag_code($deposit->bag_code_id);
				$withdrawal_code = $deposit->withdrawal_code;
				$meat_bank_details =$this->Meatbank_model->get_location_details($deposit->center);
				$meat_bank_center = $meat_bank_details->location_name;
				$days = $deposit->days;	
				if($days == 1){
					$duration = $days.'day';
				}else if($days > 1){
					$duration = $days.'days';
				}	
				if($service=='pickup'){

					$ci = get_instance();
					$ci->load->library('email');
					$config = array('protocol' => 'smtp',
						'smtp_host' => 'ssl://smtp.gmail.com',
						'smtp_port' => '465', // 465 587
						'smtp_user' => 'admin@livestock247.com',
						'smtp_pass' => 'b861N8cPchr3',
						'mailtype' => 'html',
						'charset' => 'iso-8859-1',                      
						'wordwrap' => TRUE,
					);
					$ci->email->initialize($config);
					$ci->email->set_newline("\r\n");                        
					$ci->email->from('admin@livestock247.com', 'Admin');
					$ci->email->to($email);
					$ci->email->subject(ucwords($fullname).' Your '.$size.'Kg Worth Of '.$meat.' For Deposited Is In Process For '.$service);
	
					$mail_message='<div style="width:100%;padding-bottom:20px;">
										<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
											<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
											<div style="width:100%;margin:30px">
											<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
											<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
											</div>
										</div>
										<div style="margin-top:20px;width:100%">
											<p>Dear '.ucwords($fullname).', your '.$meat.' worth of '.$size.'kg is been process for '.$service.' and will be deposited at our meatbank center '.$meat_bank_center.'</p>
											<p>kindly use withdrawal code <strong style="color:#2078BF;font-weight:bold">'.$withdrawal_code.'</strong> to collect your meat after '.$duration.'from now.</p>
										</div>
									</div>';
					$ci->email->message($mail_message);
					if($ci->email->send()){
						$this->Meatbank_model->send_sms($user_id, $fullname, $phone_number, $size, $days, $meat_bank_center, $withdrawal_code);
						$ci = get_instance();
						$ci->load->library('email');
						$config = array('protocol' => 'smtp',
							'smtp_host' => 'ssl://smtp.gmail.com',
							'smtp_port' => '465', // 465 587
							'smtp_user' => 'admin@livestock247.com',
							'smtp_pass' => 'b861N8cPchr3',
							'mailtype' => 'html',
							'charset' => 'iso-8859-1',                      
							'wordwrap' => TRUE,
						);
						$ci->email->initialize($config);
						$ci->email->set_newline("\r\n");                        
						$ci->email->from('admin@livestock247.com', 'Admin');
						$ci->email->to('methyl2007@yahoo.com');
						$ci->email->subject('Meat Got Deposited');
						$mail_message='<div style="width:100%;padding-bottom:20px;">
											<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
												<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
												<div style="width:100%;margin:30px">
												<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
												<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
												</div>
											</div>
											<div style="margin-top:20px;width:100%">
												<p>Hello team, customer with the Name: '.ucwords($fullname).' and Phone: '.$phone_number.' has his/her '.$meat.' of size '.$size.'kg with a selected bag code of '.$bag_code.' should be used to deposit her '.$meat.' to our meatbank center '.ucfirst($meat_bank_center).'</p>
												<p>The required  person in charge should make these '.$meat.' stays fresh as his/her priority, Thank you.</p>';
											'</div>
										</div>';
						$ci->email->message($mail_message);
						if($ci->email->send()){
							$this->session->unset_userdata('bag_code_id', '');
							$this->session->unset_userdata('account', '');
							$this->session->unset_userdata('transaction','');
							redirect('/');
						}
					}else{
						$this->Meatbank_model->delete_deposit($user_id, $deposit->bag_code_id);
						redirect('transaction_error');
					}	   
				}else if($service=="dropoff"){

					$ci = get_instance();
					$ci->load->library('email');
					$config = array('protocol' => 'smtp',
						'smtp_host' => 'ssl://smtp.gmail.com',
						'smtp_port' => '465', // 465 587
						'smtp_user' => 'admin@livestock247.com',
						'smtp_pass' => 'b861N8cPchr3',
						'mailtype' => 'html',
						'charset' => 'iso-8859-1',                      
						'wordwrap' => TRUE,
					);
					$ci->email->initialize($config);
					$ci->email->set_newline("\r\n");                        
					$ci->email->from('admin@livestock247.com', 'Admin');
					$ci->email->to($email);
					$ci->email->subject(ucwords($fullname).' Your '.$size.'Kg Worth Of '.$meat.' For Deposited Is In Process For '.$service);
	
					$mail_message='<div style="width:100%;padding-bottom:20px;">
										<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
											<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
											<div style="width:100%;margin:30px">
											<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
											<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
											</div>
										</div>
										<div style="margin-top:20px;width:100%">
											<p>Dear '.ucwords($fullname).', your '.$meat.' worth of '.$size.'kg is been process for '.$service.' and will be deposited at our meatbank center '.$meat_bank_center.'</p>
											<p>kindly use withdrawal code <strong style="color:#2078BF;font-weight:bold">'.$withdrawal_code.'</strong> to collect your meat after '.$duration.'from now.</p>
										</div>
									</div>';
					$ci->email->message($mail_message);
					if($ci->email->send()){
						$this->Meatbank_model->send_sms($user_id, $fullname, $phone_number, $size, $days, $meat_bank_center, $withdrawal_code);
						$ci = get_instance();
						$ci->load->library('email');
						$config = array('protocol' => 'smtp',
							'smtp_host' => 'ssl://smtp.gmail.com',
							'smtp_port' => '465', // 465 587
							'smtp_user' => 'admin@livestock247.com',
							'smtp_pass' => 'b861N8cPchr3',
							'mailtype' => 'html',
							'charset' => 'iso-8859-1',                      
							'wordwrap' => TRUE,
						);
						$ci->email->initialize($config);
						$ci->email->set_newline("\r\n");                        
						$ci->email->from('admin@livestock247.com', 'Admin');
						$ci->email->to('methyl2007@yahoo.com');
						$ci->email->subject('Meat Got Deposited');
						$mail_message='<div style="width:100%;padding-bottom:20px;">
											<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
												<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
												<div style="width:100%;margin:30px">
												<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
												<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
												</div>
											</div>
											<div style="margin-top:20px;width:100%">
												<p>Hello team, customer with the Name: '.ucwords($fullname).' and Phone: '.$phone_number.' has his/her '.$meat.' of size '.$size.'kg with a selected bag code of '.$bag_code.' should be used to deposit her '.$meat.' to our meatbank center '.ucfirst($meat_bank_center).'</p>
												<p>The required  person in charge should make these '.$meat.' stays fresh as his/her priority, Thank you.</p>';
											'</div>
										</div>';
						$ci->email->message($mail_message);
						if($ci->email->send()){
							$this->session->unset_userdata('transaction');
							redirect('/');
						}
					}else{
						$this->Meatbank_model->delete_deposit($user_id, $deposit->bag_code_id);
						redirect('transaction_error');
					}

				}								
 			
			}else{
				redirect('transaction_error');
			}	
		}	
		
	}


	    public function verify_withdrawal_transaction(){
		$reference = $this->uri->segment(2);
		if($reference == ""){
			header("Location:javascript://history.go(-1)");
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.paystack.co/transaction/verify/".rawurlencode($reference),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer sk_test_41c1144e4ddf66786d22a25af1aed53cf8712ee1", //replace this with your own test key
		"Cache-control: no-cache",
		),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		$res = json_decode($response, true);
		if($err){
		// there was an error contacting the Paystack API
		die('Curl returned error: ' . $err);
		}else{
			if($res["data"]["status"] == "success"){			
				$paystack_ref = $res["data"]["reference"];
				$amount_paid = $res["data"]["amount"];
				$transaction_time = date('h:i:s a');
				$meat_id = $this->session->userdata('meat_id');
				$meat_type = $this->session->userdata('meat_type');
				$deposited_id = $this->session->userdata('deposited_id');
				$deposited_kg = $this->session->userdata('deposited_kg');
				$meat_bank_location = $this->session->userdata('meat_bank_location');				
				$kg_withdrawn = $this->session->userdata('kg_withdrawn');
				$total_kg_left = $this->session->userdata('total_kg_left'); 
				$status = 'paid';
				$address = $res["data"]["metadata"]["custom_fields"][0]["address"];
				$user_id = $this->session->userdata('logged_in_id');
				$user = $this->Meatbank_model->get_user($user_id);
				$this->Meatbank_model->insert_withdrawal($user->id, $deposit_id, $paystack_ref,  $transaction_time, $kg_withdrawn, $status, $amount_paid);
				$this->Meatbank_model->update_deposit_withdrawal($user->id, $withdrawal_code, $total_kg_left, $address);
				$user_id = $user->id;
				$fullname = $user->name;
				$email = $user->email;
				$phone_number =$user->phone;
				$ci = get_instance();
				$ci->load->library('email');
				$config = array('protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.gmail.com',
					'smtp_port' => '465', // 465 587
					'smtp_user' => 'admin@livestock247.com',
					'smtp_pass' => 'b861N8cPchr3',
					'mailtype' => 'html',
					'charset' => 'iso-8859-1',                      
					'wordwrap' => TRUE,
				);
				$ci->email->initialize($config);
				$ci->email->set_newline("\r\n");                        
				$ci->email->from('admin@livestock247.com', 'Admin');
				$ci->email->to($email);
				$ci->email->subject('Hello '.ucwords($fullname).' '.$kg_withdrawn.'kg Worth Of '.ucfirst($meat_type).' Has Been Withdrawn');
				$mail_message='<div style="width:100%;padding-bottom:20px;">
									<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
										<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
										<div style="width:100%;margin:30px">
										<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
										<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
										</div>
									</div>
									<div style="margin-top:20px;width:100%">
										<p>Dear '.ucwords($fullname).' '.$kg_withdrawn.'kg worth of '.$meat_type.' has been withdrawn from your'.$deposited_kg.'kg worth of '.$meat_type.' to be delivered by our meat bank center '.$meat_bank_location.', to your address</p>
										<p>'.$address.' with a delivery fee of &#8358;'.$amount_paid.'</p>
									</div>
								</div>';
				$ci->email->message($mail_message);
				if($ci->email->send()){
					$this->Meatbank_model->send_sms_withdraw($user_id, $fullname, $phone_number, $kg_withdrawn, $total_kg_left, $meat_type, $amount_paid, $meat_bank_location);
					$ci = get_instance();
					$ci->load->library('email');
					$config = array('protocol' => 'smtp',
						'smtp_host' => 'ssl://smtp.gmail.com',
						'smtp_port' => '465', // 465 587
						'smtp_user' => 'admin@livestock247.com',
						'smtp_pass' => 'b861N8cPchr3',
						'mailtype' => 'html',
						'charset' => 'iso-8859-1',                      
						'wordwrap' => TRUE,
					);
					$ci->email->initialize($config);
					$ci->email->set_newline("\r\n");                        
					$ci->email->from('admin@livestock247.com', 'Admin');
					$ci->email->to('methyl2007@yahoo.com');
					$ci->email->subject('Meat Got Withdrawn');
					$mail_message='<div style="width:100%;padding-bottom:20px;">
										<div style="width:100%;background-color:#F8F8F8;padding:10px;height:150px;display:flex;justify-content:space-around">
											<div ><img src="'.base_url('assets/images/MeatBank Official Logo.png').'" width="45px" height="45px"></div>
											<div style="width:100%;margin:30px">
											<h1 style="color:#2078BF">First Online Meatbank Platform </h1>
											<h5 style="color:grey;font-size:bold;">Powered By Livestock247.com</h5>
											</div>
										</div>
										<div style="margin-top:20px;width:100%">
											<p>Hello team, customer with the Name: '.ucwords($fullname).' and Phone: '.$phone_number.' is withdrawing '.$kg_withdrawn.'kg worth of '.$meat_type.' from his or her '.$deposited_kg.'kg worth of '.$meat_type.' with a delivery fee of &#8358'.$amount.'</p>
											<p>Our meat bank center '.$meat_bank_location.' or any of our available meat bank center </p>';
										'</div>
									</div>';
					$ci->email->message($mail_message);
					if($ci->email->send()){
						$this->session->unset_userdata('withdrawal_code', '');
						$this->session->unset_userdata('account_number_or_phone', '');
						$this->session->unset_userdata('transaction', '');
						redirect('/');
					}
			    }
							
			}else{
				redirect('transaction_error');
			}	
		}	
		
	}

	public function get_meat_bank_locations(){
		$stateId = $this->uri->segment(2);
		$data = array();
		
		if($this->Meatbank_model->get_meat_bank_locations($stateId) != 1){
			
			$data['locations'] = $this->Meatbank_model->get_meat_bank_locations($stateId);
			$data['success'] =true;
		}else{
			$data['locations'] ='<option value="none">Select-Meat-Bank-Center</option>';
			$data['success'] = false;
		}
		echo json_encode($data);
        
	}

    public function orders(){
		if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user"){
			$user = $this->Meatbank_model->get_user($this->session->userdata('logged_in_id'));
			$this->load->view('includes/head');
			$this->load->view('user.php',["user"=>$user]);
			$this->load->view('includes/footer');			
		}else{
			$this->session->set_flashdata('message_danger', '<strong> Kindly put the right login details</strong>');            
			redirect('login'); 			
		}

    }

    public function deposit()
	{
		if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user"  ){
			if(!$this->session->userdata('account_number')){
				redirect('/');
			}
			$user = $this->Meatbank_model->get_user($this->session->userdata('logged_in_id'));
			$bag_code_id = $this->session->userdata('bag_code_id');
			$center = $this->session->userdata('center');
			$deposit = $this->Meatbank_model->get_deposit($user->id, $bag_code_id);
			$meat = $this->Meatbank_model->get_meat($deposit->meat_type_id);
			$total = $deposit->amount;
			$days = (int)$deposit->days;
			if($days == 1){
                $duration = $days.'day';
			}else if($days > 1){
				$duration = $days.'days';
			}
			$location_center_details = $this->Meatbank_model->get_location_details($center);
			$meat_bank_center = $location_center_details->location_name;
			$delivery_fee = $location_center_details->delivery_fee;
			$grand_total = $total + $delivery_fee;
			if($deposit->status !='paid'){
				$this->load->view('includes/head');
				$this->load->view('preview.php',["user"=>$user,"deposit"=>$deposit,"meat"=>$meat, "delivery_fee"=>$delivery_fee, "meat_bank_center"=>$meat_bank_center, "duration"=>$duration, "grand_total"=>$grand_total ]);
				$this->load->view('includes/footer');
			}else{
				redirect('/');
			}					

		}else{
			$this->session->set_flashdata('message_danger', '<strong>Kindly go ahead and login </strong>');            
			redirect('login'); 			
		}
    }


	public function withdraw()
	{

		if($this->session->userdata('logged_in_id') && $this->session->userdata('role_logged_on') && $this->session->userdata('role_logged_on') == "user"  ){
			if(!$this->session->userdata('account_number_or_phone') && $this->session->userdata('withdrawal_code')){
				redirect('/');
			}
			$user = $this->Meatbank_model->get_user($this->session->userdata('logged_in_id'));

			$size = $this->session->userdata('kg_withdrawn');
			$service = $this->session->userdata('service');
			$location_center_details = $this->Meatbank_model->get_location_details($this->session->userdata('center'));
			$user_id = $this->session->userdata('logged_in_id');
			$user = $this->Meatbank_model->get_user($user_id);
			$user_id = $user->id;
			$withdrawal_code = $this->session->userdata('withdrawal_code');
			$deposit = $this->Meatbank_model->get_user_meat_deposit($user_id, $withdrawal_code);
			if(!$this->session->userdata('address')){
				$this->session->set_userdata('address', $deposit->address);
			}
			$this->session->set_userdata('deposited_id', $deposit->id);
			$this->session->set_userdata('deposited_kg', $deposit->kg);		
			$meat = $this->Meatbank_model->get_meat($deposit->meat_type_id);
			$this->session->set_userdata('meat_id', $meat->id);	
			$this->session->set_userdata('meat_type', $meat->meat_type);	
			$total_kg_left = $deposit->reconsilation_kg - $size;
			$this->session->set_userdata('total_kg_left', $total_kg_left);	
			if($total_kg_left < $size){
				$total_kg_left = 0;
			}
			$address = $this->session->userdata('address');
			if(!$this->session->userdata('address')){
				$address =$deposit->address;
			}			
			$meat_bank_center = $location_center_details->location_name;
			$delivery_fee = $location_center_details->delivery_fee;
				$this->load->view('includes/head');
				$this->load->view('withdraw',["user"=>$user,"deposit"=>$deposit,"meat"=>$meat, "delivery_fee"=>$delivery_fee, "meat_bank_center"=>$meat_bank_center, "total_kg_left"=>$total_kg_left, "size"=>$size, "service"=>$service, "address"=>$address]);
				$this->load->view('includes/footer');
		}else{
			$this->session->set_flashdata('message_danger', '<strong>Kindly go ahead and login </strong>');            
			redirect('login'); 			
		}
    }
	
	public function transaction_error(){
		$this->load->view('includes/head');
		$this->load->view('transaction_error.php');
		$this->load->view('includes/footer');		
	}

    public function get_all_pending_orders_pagination(){
		$user = $this->Meatbank_model->get_user($this->session->userdata('logged_in_id'));
		$user_id = $user->id;		
        $config = array();
		$config['base_url'] =base_url().'orders';
        $config["total_rows"] = $this->Meatbank_model->get_count($user_id);
        $config["per_page"] = 4;
        $config["use_page_numbers"] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config["next_link"] = 'Next';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = 'Previous';
        $config["prev_tag_open"] = '<li>';
        $config["prev_tag_close"] = '</li>';
        $config["cur_tag_open"] = '<li ><a href="#" class="activ_me">';
        $config["cur_tag_close"] = '</a></li>';
        $config["num_tag_open"] = '<li>';
        $config["num_tag_close"] = '</li>';
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $start = ($page - 1) * $config["per_page"];		
        $output = array(
            'pagination_link' => $this->pagination->create_links(),
            'all_pending_orders_table' => $this->Meatbank_model->get_all_orders($config["per_page"], $page, $user_id)
        );
        echo json_encode($output);
        
    } 	
	public function delete_deposit_order(){
		$depositId = $this->uri->segment(2);
		$data = array();
		if($this->Meatbank_model->delete_deposit_order($depositId) == 1){
			$data['success'] =true;
		}else{
			$data['success'] = false;
		}
		echo json_encode($data);
        
	}
    // Logout from the application
    public function logout() {	
		$data = array();
		if($this->input->server('REQUEST_METHOD') === 'POST'){
			$this->session->unset_userdata('role_logged_on', '');
			$this->session->unset_userdata('logged_in_id', '');
			$this->session->unset_userdata('fullname', '');
			$this->session->unset_userdata('withdrawal_code', '');
			$this->session->unset_userdata('account_number_or_phone', '');
			$this->session->unset_userdata('bag_code_id', '');
			$this->session->unset_userdata('account_number', '');					
			$data['success'] = true;
		}else{		
			$this->session->unset_userdata('role_logged_on', '');
			$this->session->unset_userdata('logged_in_id', '');
			$this->session->unset_userdata('fullname', '');
			$this->session->unset_userdata('withdrawal_code', '');
			$this->session->unset_userdata('account_number_or_phone', '');
			$this->session->unset_userdata('bag_code_id', '');
			$this->session->unset_userdata('account_number', '');				
			redirect('login');
		}
		echo json_encode($data);
    }	
  
}   
