-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 04, 2022 at 09:40 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meatbank`
--

-- --------------------------------------------------------

--
-- Table structure for table `bag_codes`
--

CREATE TABLE `bag_codes` (
  `id` int(11) NOT NULL,
  `bag_code_name` varchar(30) NOT NULL,
  `meat_bank_location_id` int(11) NOT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bag_codes`
--

INSERT INTO `bag_codes` (`id`, `bag_code_name`, `meat_bank_location_id`, `status`) VALUES
(1, 'BG-01', 1, 'used'),
(2, 'BG-02', 1, NULL),
(3, 'BG-03', 2, NULL),
(4, 'BG-04', 2, 'used');

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `kg` varchar(30) NOT NULL,
  `reconsilation_kg` varchar(30) NOT NULL,
  `amount` varchar(30) NOT NULL,
  `meat_type_id` int(11) NOT NULL,
  `center` varchar(30) NOT NULL,
  `bag_code_id` varchar(30) NOT NULL,
  `days` varchar(30) NOT NULL,
  `withdrawal_code` varchar(100) DEFAULT NULL,
  `transaction_time` time DEFAULT NULL,
  `paystack_ref` varchar(50) DEFAULT NULL,
  `service` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit`
--

INSERT INTO `deposit` (`id`, `user_id`, `kg`, `reconsilation_kg`, `amount`, `meat_type_id`, `center`, `bag_code_id`, `days`, `withdrawal_code`, `transaction_time`, `paystack_ref`, `service`, `status`, `address`) VALUES
(15, 3, '3', '3', '600', 1, '2', '4', '4', 'rnxqsf', '05:11:44', 'T691926919941611', 'pickup', 'paid', 'hgxfcbvnb'),
(43, 3, '1', '1', '250', 2, '1', '1', '5', NULL, NULL, NULL, 'pickup', 'pending', 'Sdfghfuresdk');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `delivery_fee` varchar(50) DEFAULT NULL,
  `state_id` int(11) NOT NULL,
  `state_email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name`, `delivery_fee`, `state_id`, `state_email`) VALUES
(1, 'oko-oba', '1000', 24, NULL),
(2, 'lekki', '1200', 24, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meat`
--

CREATE TABLE `meat` (
  `id` int(11) NOT NULL,
  `meat_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meat`
--

INSERT INTO `meat` (`id`, `meat_type`) VALUES
(1, 'beef'),
(2, 'offers');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'superadmin'),
(2, 'admin'),
(3, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `id` int(100) NOT NULL,
  `date` varchar(100) CHARACTER SET utf8 NOT NULL,
  `message` varchar(100) CHARACTER SET utf8 NOT NULL,
  `recipient` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `date`, `message`, `recipient`, `user`) VALUES
(1, '1646665532', '<p>Kindly use this bzfxlj to collect your meat worth of 2kg at meatbank center lekki after 2day/days', 'To Our Meatank Customer At livestock27 Mutiu Adepoju', '3'),
(2, '1646668888', '<p>Kindly use this ajymbs to collect your meat worth of 2kg at meatbank center lekki after 2day/days', 'To Our Meatank Customer At livestock27 Mutiu Adepoju', '3'),
(3, '2022-04-03 04:54:06am', 'Kindly use this widthdrawal code <strong style=\"color:#2078BF;font-weight:bold\">ulmuas</strong> to c', 'To Our Meatank Customer At livestock27 Mutiu Adepoju', '3'),
(4, '2022-04-04 09:58:07am', 'Kindly use this widthdrawal code <strong style=\"color:#2078BF;font-weight:bold\">svdirc</strong> to c', 'To Our Meatank Customer At livestock27 Mutiu Adepoju', '3'),
(5, '2022-04-04 15:38:54pm', 'Kindly use this widthdrawal code <strong style=\"color:#2078BF;font-weight:bold\">neunsd</strong> to c', 'To Our Meatank Customer At livestock27 Mutiu Adepoju', '3');

-- --------------------------------------------------------

--
-- Table structure for table `sms_withdrawal`
--

CREATE TABLE `sms_withdrawal` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `message` text NOT NULL,
  `recepient` varchar(100) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `state_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`) VALUES
(1, 'Abia State'),
(2, 'Adamawa State'),
(3, 'Akwa Ibom State'),
(4, 'Anambra State'),
(5, 'Bauchi State'),
(6, 'Bayelsa State'),
(7, 'Benue State'),
(8, 'Borno State'),
(9, 'Cross River State'),
(10, 'Delta State'),
(11, 'Ebonyi State'),
(12, 'Edo State'),
(13, 'Ekiti State'),
(14, 'Enugu State'),
(15, 'FCT'),
(16, 'Gombe State'),
(17, 'Imo State'),
(18, 'Jigawa State'),
(19, 'Kaduna State'),
(20, 'Kano State'),
(21, 'Katsina State'),
(22, 'Kebbi State'),
(23, 'Kwara State'),
(24, 'Lagos State'),
(25, 'Nasarawa State'),
(26, 'Niger State'),
(27, 'Ogun State'),
(28, 'Ondo State'),
(29, 'Osun State'),
(30, 'Oyo State'),
(31, 'Plateau State'),
(32, 'Rivers State'),
(33, 'Sokoto State'),
(34, 'Taraba State'),
(35, 'Yobe State'),
(36, 'Zamfara State');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `account_number` varchar(50) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `role_id` mediumint(10) DEFAULT NULL,
  `token` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `account_number`, `password`, `role_id`, `token`, `status`) VALUES
(1, NULL, 'admin@adminlivestock247.com', NULL, NULL, '$2y$10$n6mLjzT41CVRUaMt.3Gk0OxqJgtVlsQTivwoBekK6bsgCiGGqWvkq', 1, NULL, 'verified'),
(3, 'Mutiu Adepoju', 'methyl2007@gmail.com', '09036561101', '5460223509', '$2y$10$0RgUDIWLMbb8bcZE2osYIe/XD8Zxv6JHUiDuRePjmH6Ko1a0aTJ9y', 3, 'xg3qb1dr', 'verified'),
(13, 'Bola Ayanwuyi', 'methyl2007@yahoo.com', '08188373898', 'MB-632366', '$2y$10$m/RGVOEv2RsvSbjWdo.hTewrTwSugk3Ys0iZecrSg/pj9Khg/IYK2', 3, 'xa7cb61d', 'unverified');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` int(11) NOT NULL,
  `amount` varchar(30) DEFAULT NULL,
  `kg_withdrawn` varchar(30) NOT NULL,
  `paystack_ref` varchar(50) DEFAULT NULL,
  `deposited_id` varchar(20) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `transaction_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bag_codes`
--
ALTER TABLE `bag_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meat_type_id` (`meat_type_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meat`
--
ALTER TABLE `meat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_withdrawal`
--
ALTER TABLE `sms_withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bag_codes`
--
ALTER TABLE `bag_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meat`
--
ALTER TABLE `meat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sms_withdrawal`
--
ALTER TABLE `sms_withdrawal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deposit`
--
ALTER TABLE `deposit`
  ADD CONSTRAINT `fk_to_meat_type_id` FOREIGN KEY (`meat_type_id`) REFERENCES `meat` (`id`),
  ADD CONSTRAINT `fk_to_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
